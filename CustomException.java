import java.util.*;

class NewException extends Exception{
    public NewException(String s){
        super(s);
    }
}


class CustomException{
    public static void main(String args[]){
        try{
            throw new NewException("First Exception");
        }
        catch (NewException ex){
            System.out.println("Exception Handled!");
            System.out.println(ex.getMessage());
        }
    }
}

