import java.util.*;

abstract class Shape {
    abstract double area();
}

class Triangle extends Shape {
    double a,b,c;
    Triangle(double x,double y,double z){
        a=x;b=y;c=z;
    }

    double area(){
        double s=(a+b+c)/2;//semi parameter
        double h=Math.sqrt(s*(s-a)*(s-b)*(s-c));
        return h;
    }
}

class Circle extends Shape {
    double rad;
    Circle(double r){
        rad=r;
    }

    double area(){
        double a=Math.PI* Math.pow(rad,2);
        return a;
    }
}

class Rectangle extends Shape {
    double a,b;
    Rectangle(double x, double y){
        a=x;
        b=y;
    }
    double area(){
        double ar=a*b;
        return ar;
    }
}

class Square extends Shape{
    double s;
    Square(double l){
        s=l;
    }
    double area(){
        double a=s*s;
        return a;
    }
}

class DifferentShapes {
    public static void main(String args[]){
        Triangle t=new Triangle(4,5,6);
        System.out.println("Area of the triangle: "+t.area());
        Circle c=new Circle(7);
        System.out.println("Area of the circle: "+c.area());
        Rectangle r=new Rectangle(6,8);
        System.out.println("Area of the rectangle: "+r.area());
        Square s=new Square(9);
        System.out.println("Area of the square: "+s.area());
    }
}



    
